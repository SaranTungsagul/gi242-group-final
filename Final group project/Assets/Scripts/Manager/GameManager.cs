﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

namespace Manager
{
    public class GameManager : MonoBehaviour
    {

        [SerializeField] private Button startButton;
        [SerializeField] private Button quitButton;
        [SerializeField] private RandomSpawn randomspawn;
        [SerializeField] private RectTransform dialog;
        [SerializeField] private RectTransform endscene;


        public static GameManager Instance { get; private set; }


        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            DontDestroyOnLoad(this);
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(dialog != null, "dialog cannot be null");

            endscene.gameObject.SetActive(false);
            startButton.onClick.AddListener(OnStartButtonClicked);
            quitButton.onClick.AddListener(OnquitButtonClicked);

        }

        public void Start()
        {
            
        }


        private void StartGame()
        {
            ScoreManager.Instance.Init(this);
            SpawnRandomSpawn();
        }

        public void Lost()
        {
            endscene.gameObject.SetActive(true);
            Time.timeScale = 0f;
        }

        private void OnStartButtonClicked()
        {
            dialog.gameObject.SetActive(false);
            StartGame();
            
        }

        private void SpawnRandomSpawn()
        {
            var RandomSpawn = Instantiate(randomspawn);
        }

        private void OnquitButtonClicked()
        {
            Application.Quit();
        }
    }

}

