﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

namespace Manager 
{
    public class SoundManager : MonoBehaviour
    {
        [SerializeField] private SoundClip[] soundClips;
        [SerializeField] private AudioSource audioSource;

        public static SoundManager Instance { get; private set; }

        public enum Sound
        {
            BGM,
            GotPoint,
            MissedPoint,
            EndSound
            
        }

        [Serializable]
        public class SoundClip
        {
            public Sound sound;
            public AudioClip audioClip;
            [Range(0,1)] public float soundVolume;
            public bool loop = false;
            [HideInInspector]
            public AudioSource audioSource;
        }

        public void Awake()
        {

            if (Instance == null)
            {
                Instance = this;
            }
            DontDestroyOnLoad(this);

        }

        public void Play(Sound sound)
        {
            var soundClip = GetSoundClip(sound);
            if (soundClip.audioSource == null)
            {
                soundClip.audioSource = gameObject.AddComponent<AudioSource>();
            }
            soundClip.audioSource.clip = soundClip.audioClip;
            soundClip.audioSource.volume = soundClip.soundVolume;
            soundClip.audioSource.loop = soundClip.loop;
            soundClip.audioSource.Play();

        }
        

        private SoundClip GetSoundClip(Sound sound)
        {
            foreach (var soundClip in soundClips)
            {
                if (soundClip.sound == sound)
                {
                    return soundClip;
                }

            }
            return default(SoundClip);
        }
    }

}

