﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitch : MonoBehaviour
{
    public void playGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        //Go to Normal Mode
    }
    public void Warp()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 2);
        //Go to Hard Mode
    }
}