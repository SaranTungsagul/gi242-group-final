﻿using TMPro;
using UnityEngine;

namespace Manager
{
    public class ScoreManager : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private TextMeshProUGUI FinalScoreText;

        public static ScoreManager Instance { get; private set; }

        private int playerScore;
        private int missedScore;

        public void Init(GameManager gameManager)
        {
            SetScore(0);
        }
        public void SetScore(int score) 
        {
            if(missedScore <= 3)
            {
                playerScore += score;
                scoreText.text = $"Score : {playerScore}";
                FinalScoreText.text = $"You got : {playerScore}";
            }
            

        }
        public void CheckScore(int score)
        {
            missedScore += score;
            if (missedScore >= 3)
            {
                GameManager.Instance.Lost();
            }
        }

        private void Awake()
        {
            Debug.Assert(scoreText != null, "scoreText cannot null");

            if (Instance == null)
            {
                Instance = this;
            }
            DontDestroyOnLoad(this);
        }

    }
}

