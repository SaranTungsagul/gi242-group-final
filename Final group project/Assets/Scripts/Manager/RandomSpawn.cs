﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Manager
{
    public class RandomSpawn : MonoBehaviour
    {
        public Transform[] spawnPoints;
        public GameObject[] enemyPrefabs;

        private float Timer = 0.0f;
        private float SpawnTimer = 3.0f;


        void Start()
        {

        }

        void Update()
        {
            Timer += Time.deltaTime;
            if (Timer > SpawnTimer)
            {
                int randEnemy = Random.Range(0, enemyPrefabs.Length);
                int randSpawnPoint = Random.Range(0, spawnPoints.Length);

                Instantiate(enemyPrefabs[0], spawnPoints[randSpawnPoint].position, transform.rotation);
                Timer = 0.0f;
                SpawnTimer *= 0.98f;

            
            }
        }
    }
}

