﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;
using Manager;

namespace Enemy
{
    public class Rock : MonoBehaviour
    {

        void Update()
        {

        }

        //Done : When rock hit player, rock destroy | When rock hit ground, rock destroy
        private void OnTriggerEnter(Collider other) 
        {

            if (other.CompareTag("Player"))
            {
                ScoreManager.Instance.SetScore(1);
                SoundManager.Instance.Play(SoundManager.Sound.GotPoint);
                Destroy(this.gameObject);

                return;
            }

            else if (other.CompareTag("DestroyMissed"))
            {
                SoundManager.Instance.Play(SoundManager.Sound.MissedPoint);
                Destroy(this.gameObject); 
                ScoreManager.Instance.CheckScore(1); 
                ScoreManager.Instance.SetScore(-1); 
                return;
               
            }

        }
    }
}

