﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AngularVelocity : MonoBehaviour
{
    [SerializeField] private Vector3 angularVelocity;
    private Rigidbody rb;
    
    private float Timer = 0.0f;
    private float SpinTimer = 1.0f;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        Timer += Time.deltaTime;
        if (Timer > SpinTimer)
        {
            rb.angularVelocity = angularVelocity;
            Timer = 0.0f;
            SpinTimer *= 0.95f;
        }
    }
}
