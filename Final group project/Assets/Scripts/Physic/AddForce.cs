﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddForce : MonoBehaviour 
{

    [SerializeField] private int speed = 20;
    // Update is called once per frame
    void Update()
    {
        if ( Input.GetKey(KeyCode.D))
        {
            GetComponent<Rigidbody>().velocity = new Vector3(speed,0,0);
        }
        if ( Input.GetKey(KeyCode.A))
        {
            GetComponent<Rigidbody>().velocity = new Vector3(-speed,0,0);
        }
        if ( Input.GetKey(KeyCode.A) == false && Input.GetKey(KeyCode.D) == false)
        {
            GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        }
    }
}
